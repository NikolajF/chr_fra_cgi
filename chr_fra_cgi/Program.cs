﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP; //Namespace 
using System.Configuration;
using System.Data;
using System.Threading;
using LE34.Data.Access;
using System.Diagnostics;
using System.IO;

using System.IO.Compression;




namespace chr_fra_cgi
{
	class Program
	{
		private static string progsti = System.Reflection.Assembly.GetExecutingAssembly().Location;
		private static string nysti = "";
		private static string glsti = "";
		private static string logfil = "";
		private static bool opdat_tæthed = true;
		private static bool fejl = false;
		
		class Fil
		{
			public string filnavn { get; set; }
			public bool ændret { get; set; }
			public string tabelnavn { get; set; }
		}
		static List<Fil> filer = new List<Fil>();

		static void Main(string[] args)
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
			progsti = progsti.Substring(0, progsti.LastIndexOf("\\") + 1) + "data\\";
			// opret mapper og sæt sti-variable
			nysti = progsti + "ny";
			glsti = progsti + "gl";
			// sørg for at undermapperne ny og gl findes
			Directory.CreateDirectory(nysti);
			Directory.CreateDirectory(glsti);
			nysti += "\\";
			glsti += "\\";
			logfil = progsti + "chr_fra_cgi.log"; // dette er program-loggen, der er en anden fil med session-log fra download-processen


			skrivLog("programstart");


	
			download();
			pak_ud();
			tjek_ændringer();
			bool indlæst = indlæs_nye();
			if (indlæst == false)
			{
				skrivLog("intet nyt");
				System.Environment.Exit(0);
			}
			
			
			if (opdat_tæthed && indlæst) 
				opdater_tæthed();	
			
			// kopier til de øvrige servere
			// første operation foregår på fvst-test
			// der skal kopieres til 
			// srv-postgis, så landbrug og fødevarer kan få fat i data
			// t26 til den gamle webgis - dette bør kunne slås fra i config

			string[] KopierTil = ConfigurationManager.AppSettings["KopierTil"].Split(';');
			foreach (string kop in KopierTil)
			{
				switch (kop)
				{
					case "lf":
						// dyrearter, chr_samlet
						kopier_lf();
						break;
					case "t26":
						// dyreart, chr_samlet, chr_ophoert
						kopier_t26();
						break;

				}

			}
			if (fejl == false)
				skrivLog("program afsluttet korrekt");

			/*
			her er alle trinnene i operationen
			download den nye zip-fil
			tjek om filen er ændret i forhold til forrige 
			unzip 
			indlæs hver tabel i temp-skema - tabellerne skal være oprettet i forvejen, og indekserede. der er 4, aktive og nedlagte chr, dyrearter og virksomhedstyper
			find de aktive chr-numres kommuner - det må være nok at gøre det med de nye chr-numre eller dem hvor stald_koordinater / geometri er ændret, det tager nemlig lang tid
			chr_antal - saml antallene i de fire kolonner og for alle besætninger til 
			chr_kom
			tæthedstabeller

			
			*/
		}
		static void download()
		{
			skrivLog("downloader");
			//Get Files - ideally you would want to wrap this into a try...catch 
			//and possible execute more than once if you can't connect the first time. 
			SessionOptions sessionOptions = new SessionOptions
			{
				Protocol = Protocol.Sftp,
				HostName = "78.41.244.108", //hostname e.g. IP: 192.54.23.32, or mysftpsite.com
				UserName = "le34",
				Password = "Wwjb4Hlf81U", 
				PortNumber = 22,
				SshHostKeyFingerprint = "ssh-rsa 2048 4e:86:82:14:64:2d:c7:da:e5:20:c8:2e:8f:8b:b0:2a" 
			};
			using (Session session = new Session())
			{
				session.SessionLogPath =  progsti + "chr_fra_cgi_download.log";
				session.Open(sessionOptions); //Attempts to connect to your sFtp site
				//Get Ftp File
				string remoteFilePath ="/out/le34_udtraek.zip";
				string localFilePath = progsti + "le34_udtraek.zip";
				TransferOptions transferOptions = new TransferOptions(); 
				transferOptions.TransferMode = TransferMode.Binary; //The Transfer Mode - Automatic, Binary, or Ascii 
				transferOptions.FilePermissions = null;  //Permissions applied to remote files; 
				//<em style="font-size: 9pt;">null for default permissions.  
				//Can set <em style="font-size: 9pt;">user, Group, or other Read/Write/Execute permissions.  
				transferOptions.PreserveTimestamp = false;  //Set last write time of destination file 
				//to that of source file - basically change the timestamp to match destination and source files.    
				transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;
     
				TransferOperationResult transferResult;
				//the parameter list is: remote Path, Local Path with filename 
				//(optional - if different from remote path), Delete source file?, transfer Options  
				transferResult = session.GetFiles("/" + remoteFilePath, localFilePath, false, transferOptions); 
				//Throw on any error 
				transferResult.Check();
				//Log information and break out if necessary  
			}	
		}

		static void pak_ud()
		{
			skrivLog("pak_ud");
			// overfør ændrede filer til glsti og tøm nysti
			// hvis der findes filer i glsti, bliver de bare liggende
			string[] nyfiler = Directory.GetFiles(nysti);
			foreach (string f in nyfiler)
			{
				string gf = glsti + f.Substring(f.LastIndexOf("\\") + 1);
				if (File.Exists(gf))
					File.Delete(gf);
				Directory.Move(f, gf);
			}
			// pak ud til nysti
			ZipArchive z = ZipFile.OpenRead(progsti + "le34_udtraek.zip");
			foreach (ZipArchiveEntry entry in z.Entries)
			{
				// only grab files that end in .csv
				if (entry.FullName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
				{
					// this extracts the file
					entry.ExtractToFile(Path.Combine(nysti, entry.Name));
					Fil a = new Fil();
					a.filnavn = entry.Name;
					// find tabelnavnet en gang for alle
					if (entry.Name.Contains("dyrearter"))
						a.tabelnavn = "chr_dyrearter";
					else if (entry.Name.Contains("virksomhedsarter"))
						a.tabelnavn = "chr_virksomhedsarter";
					else if (entry.Name.Contains("ophoert"))
						a.tabelnavn = "chr_ophoert";
					else if (entry.Name.Contains("aktive"))
						a.tabelnavn = "chr_samlet";

					filer.Add(a);
				}
			}
		}
		static void tjek_ændringer()
		{
			skrivLog("tjek_ændringer");
			foreach (Fil f in filer)
			{
				if (File.Exists(glsti + f.filnavn))
				{
					string fg = File.ReadAllText(glsti + f.filnavn);
					string fny = File.ReadAllText(nysti + f.filnavn);
					if (fg != fny)
					{
						f.ændret = true;
					}
				}
				else // hvis den ikke findes i forvejen, er den ny
					f.ændret = true;
				if (f.ændret == true && f.tabelnavn == "chr_samlet")
					opdat_tæthed = true;
				//skrivLog(f.filnavn + "  " + f.ændret.ToString());
			}
		}
		static bool indlæs_nye()
		{
			skrivLog("indlæs_nye");
			bool svar = true;
			DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);
			DataBase tmp = new DataBase(ConfigurationManager.ConnectionStrings["fvst_temp"].ConnectionString);
			try
			{
				db.BeginTransaction();
				foreach (Fil f in filer)
				{
                    Console.WriteLine(f.tabelnavn + "  -  " + f.ændret);
                    if (f.ændret) // så skal den indlæses
					{
						// truncate temptabel
						tmp.SetSQLStatement("truncate temp." + f.tabelnavn);
						////skrivLog("tmp", tmp.GetSQLStatement());
						tmp.Execute();
						// indlæs i temptabel lokalt
						tmp.SetSQLStatement("copy temp." + f.tabelnavn + " from '" + nysti + f.filnavn + "' DELIMITERS ';' CSV QUOTE E'\\b' ENCODING 'LATIN1'");
						////skrivLog("tmp", tmp.GetSQLStatement());
						tmp.Execute();
						// indsæt i rette tabeller på  fvst-test
						switch (f.tabelnavn)
						{
							case "chr_samlet":
								// truncate live-tabel 
								db.SetSQLStatement("truncate chr." + f.tabelnavn);
								////skrivLog("chr", db.GetSQLStatement());
								db.Execute();
//								db.SetSQLStatement("insert into chr.chr_samlet(chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor) select chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor from temp.chr_samlet");
								db.SetSQLStatement(@"insert into chr.chr_samlet(chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, komnr) 
									select * 
									FROM dblink('" + ConfigurationManager.ConnectionStrings["temp_link"].ConnectionString + @"','select chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, komnr from temp.chr_samlet')
									AS k(chr_nr integer,
									bes_nr integer,
									bruger character varying,
									dyreart integer,
									virk_art integer,
									virk_tekst character varying,
									antal_dyr1 integer,
									antal_dyr2 integer,
									antal_dyr3 integer,
									antal_dyr4 integer,
									vej character varying,
									bynavn character varying,
									postnr integer,
									postby character varying,
									stald_x_koor double precision,
									stald_y_koor double precision,
									x_koor double precision,
									y_koor double precision,
									komnr integer)");
								////skrivLog("chr", db.GetSQLStatement());
								db.Execute();
								// og så skal der oprettes geometri
								db.SetSQLStatement("update chr.chr_samlet set geom = st_geomfromtext('Point(' || stald_x_koor || ' ' || stald_y_koor ||')',25832) where stald_x_koor is not null and stald_x_koor <>0");
								////skrivLog("chr", db.GetSQLStatement());
								db.Execute();

								break;
							case "chr_ophoert":
								// truncate live-tabel 
								db.SetSQLStatement("truncate chr." + f.tabelnavn);
								////skrivLog("chr", db.GetSQLStatement());
								db.Execute();
								db.SetSQLStatement(@"insert into chr.chr_ophoert(chr_nr, stald_x_koor, stald_y_koor) 
									select *
									FROM dblink('" + ConfigurationManager.ConnectionStrings["temp_link"].ConnectionString + @"','SELECT chr_nr, stald_x_koor, stald_y_koor
									FROM temp.chr_ophoert')
									AS k(chr_nr integer,
									stald_x_koor double precision,
									stald_y_koor double precision)");
								//skrivLog("chr", db.GetSQLStatement());
								db.Execute();
								// og så skal der oprettes geometri
								db.SetSQLStatement("update chr.chr_ophoert set geom = st_geomfromtext('Point(' || stald_x_koor || ' ' || stald_y_koor ||')',25832) where stald_x_koor is not null and stald_x_koor <>0");
								//skrivLog("chr", db.GetSQLStatement());
								db.Execute();
								break;
							case "chr_dyrearter":
//								// sæt chr_aktuel til true på dem der findes i den aktuelle leverance og false på øvrige
//								string art = tmp.SingleSelect<string>("select string_agg(dyreartkode::text,',') from temp.chr_dyrearter");
//								////skrivLog("tmp", tmp.GetSQLStatement());
//								//db.SetSQLStatement("update chr.dyrearter set chr_aktuel =case when id in(select dyreartkode from temp.chr_dyrearter) then true else false end");
//								//db.Execute();
//								db.SetSQLStatement("update chr.dyrearter set chr_aktuel =case when id in(" + art + ") then true else false end");
//								//skrivLog("chr", db.GetSQLStatement());
//								db.Execute();
//								// tilføj endelig eventuelle nye
//								//db.SetSQLStatement("insert into chr.dyrearter(id, navn, aktiv, chr_aktuel) select dyreartkode, dyrearttekst, false, true from temp.chr_dyrearter where dyreartkode not in (select id from chr.dyrearter)");
//								//db.Execute();
//								string dk = db.SingleSelect<string>("select string_agg(id::text, ',') from chr.dyrearter");
//								if (dk == null)
//									dk = "";
//								else
//									dk= " where dyreartkode not in (" + dk + ")";
//								db.SetSQLStatement(@"insert into chr.dyrearter(id, navn, aktiv, chr_aktuel) 
//									select *
//									FROM dblink('" + ConfigurationManager.ConnectionStrings["temp_link"].ConnectionString + @"','select dyreartkode, dyrearttekst, false, true
//									FROM temp.chr_dyrearter" +  dk + @"')  
//									AS k(dyreartkode integer,
//									dyrearttekst character varying, 
//									aktiv boolean, 
//									chr_aktuel boolean)");
//								//skrivLog("chr", db.GetSQLStatement());
//								db.Execute();

		
								/******************* ny måde at gøre det på *****************************/
								// alle rækker fra begge tabeller indlæses, og dernæst løbes de igennem og nødvendige opdateringer bliver udført
								/* mulige kombinationer
								 * ny i temp
								 * mangler i temp
								 * ændret i temp
								
								 */
								db.SetSQLStatement(@"select * from chr.dyrearter a, 
									(select * from dblink('host=192.168.34.210 port=5433 dbname=fvst user=postgres password=Zxcv1234','select dyreartkode, dyrearttekst
										FROM temp.chr_dyrearter')  
										AS k(dyreartkode integer,
										dyrearttekst character varying)) b 
									where a.id=b.dyreartkode or a.id is null or b.dyreartkode is null order by a.id, b.dyreartkode");
								db.Query();
								DataTable tbl = db.Result;
								foreach (DataRow r in tbl.Rows)
								{
									if (r["dyreartkode"].ToString() =="") // findes ikke i aktuelle
									{
										if (r.Field<bool>("chr_aktuel") == true) // aktiv forbliver uændret - så for at finde dem der aktuelt kan bruges skal man se på både aktiv og chr_aktuel
										{
											db.SetSQLStatement("update chr.dyrearter set chr_aktuel =false where id =" + r["id"].ToString());
											db.Execute();
										}
									}
									else if (r["id"].ToString() == "") // ny, skal tilføjes
									{
										db.SetSQLStatement("insert into chr.dyrearter (id, navn, aktiv, chr_aktuel) values(" + r["dyreartkode"].ToString() + ",'" + r["dyrearttekst"].ToString() + "', false, true)");
										db.Execute();
									}
									else if (r["dyreartkode"].ToString() == r["id"].ToString()) // findes begge steder
									{
                                        //if (r["id"].ToString() == "56")
                                        //{
                                        //    Console.WriteLine("56");
                                        //    Console.WriteLine(r.Field<bool>("chr_aktuel") == false);
                                        //}
                                        // er navnet ændret eller status?
                                        if (r.Field<bool>("chr_aktuel") == false || r["navn"].ToString() != r["dyrearttekst"].ToString() )
										{
                                            //if (r["id"].ToString() == "56")
                                            //    Console.WriteLine("56 opdateres");
                                            db.SetSQLStatement("update chr.dyrearter set chr_aktuel = true, navn = '" + r["dyrearttekst"].ToString() + "' where id =" + r["id"].ToString());
											db.Execute();
										}
									}
								}


								break;

						}
					}
				}
				// dyrearter skal have sat aktiv på grundlag af anvendelsen i chr_samlet
				//db.SetSQLStatement("update chr.dyrearter set aktiv = false");
				//db.Execute();
				//db.SetSQLStatement("update chr.dyrearter set aktiv = true where id in(select dyreart from chr.chr_samlet)");
				db.SetSQLStatement("update chr.dyrearter set aktiv = case when id in(select dyreart from chr.chr_samlet) then true else false end");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				db.CommitTransaction();
			}
			catch (Exception e)
			{
				db.RollbackTransaction();
				svar = false;
				skrivLog("indlæsning fejlet: ", e.Message + "\r\nStacktrace: " + e.StackTrace);
				fejl = true;
			}
			db.Close();
			return svar;
		}

		static string fyld_kolonner(ref DataBase db, DataRow r)
		{
			string val = "(";
			string komma = "";
			foreach (DataColumn c in db.Result.Columns)
			{
				//Console.WriteLine(c + "   " + c.DataType);
				if (c.ColumnName != "id")
				{
					switch (c.DataType.ToString())
					{
						case "System.String":
							//if (c.ColumnName != "virk_art_gl")
							//{
							if (r[c].ToString() == "")
								val += komma + "null";
							else
								val += komma + "'" + r[c].ToString().Replace("'", "''") + "'";
							//}
							break;
						case "System.Int32":
						case "System.Double":
							if (r[c].ToString() == "")
								val += komma + "0";
							else
								val += komma + r[c].ToString();
							break;
						case "System.DateTime":
							if (r[c].ToString() == "")
								val += komma + "null";
							else
							val += komma + "'" + r[c].ToString() + "'";
							break;
						default:
							skrivLog(c + "   " + c.DataType);
							Console.Read();
							break;
					}
					komma = ",";
				}
			}
			val += ")";
			return val;
		}
		static void kopier_lf()
		{
			skrivLog("kopier_lf");
			DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);
			DataBase ud = new DataBase(ConfigurationManager.ConnectionStrings["srv-postgis-lf"].ConnectionString);

			// først chr_samlet
			// NB: komnr tages ikke med
			string sql = "insert into chr_samlet(chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, geom) values ";
			string val = "";
			int nr = 0;
			try
			{
				ud.BeginTransaction();
				ud.SetSQLStatement("truncate chr_samlet");
				skrivLog("lf", ud.GetSQLStatement());
				ud.Execute();
				db.SetSQLStatement("select chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr3, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, geom from chr.chr_samlet");
				//skrivLog("chr", db.GetSQLStatement());
				db.Query();
				foreach (DataRow r in db.Result.Rows)
				{
					nr++;
					if (val != "") val += ",";
					val += fyld_kolonner(ref db, r);


					if (nr % 100 == 0 || nr == db.Result.Rows.Count) // få også de sidste med i et hug
					{
						ud.SetSQLStatement(sql + val);
						ud.Execute();
						val = "";
					}
				}
				// og så dyrearter - bare en præcis kopi af tabellen på fvst. det skal læses og skrives linje for linje, men der er ikke flere data end at de kan køres i en enkelt forespørgsel
				ud.SetSQLStatement("truncate dyrearter");
				skrivLog("lf", ud.GetSQLStatement());
				ud.Execute();
				sql = "insert into dyrearter(id, navn, aktiv, chr_aktuel) values ";
				db.SetSQLStatement("select * from chr.dyrearter");
				//skrivLog("chr", db.GetSQLStatement());
				db.Query();
				foreach (DataRow r in db.Result.Rows)
				{
					val += ",("+ r["id"].ToString() + ",'" + r["navn"].ToString() + "'," + r["aktiv"].ToString().ToLower() + "," + r["chr_aktuel"].ToString().ToLower() + ")";
				}
				ud.SetSQLStatement(sql + val.Substring(1));
				skrivLog("lf", ud.GetSQLStatement());
				ud.Execute();

				//string dyr = ud.SingleSelect<string>("select string_agg(id::text, ',') as ids from dyrearter");
				//if (dyr != null && dyr != "")
				//{
				//	db.SetSQLStatement("select * from chr.dyrearter where id not in(" + dyr + ")");
				//	//skrivLog("chr", db.GetSQLStatement());
				//	db.Query();
				//	foreach (DataRow r in db.Result.Rows)
				//	{
				//		ud.SetSQLStatement("insert into dyrearter(id, navn, aktiv) values(" + r["id"].ToString() + ",'" + r["navn"].ToString() + "'," + r["aktiv"].ToString().ToLower() + ")");
				//		skrivLog("lf", ud.GetSQLStatement());
				//		ud.Execute();
				//	}
				//}
				//// dernæst opdatering af aktiv
				//ud.SetSQLStatement("update dyrearter set aktiv = case when id in(select distinct dyreart from chr_samlet) then true else false end");
				//skrivLog("lf", ud.GetSQLStatement());
				//ud.Execute();

				ud.CommitTransaction();
			}
			catch (Exception e)
			{
				ud.RollbackTransaction();
				skrivLog("indlæsning fejlet: ", e.Message + "\r\nStacktrace: " + e.StackTrace);
				fejl = true;
			}
			ud.Close();
			db.Close();
		}
		static void kopier_t26()
		{
			skrivLog("kopier_t26");
			DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);
			DataBase ud = new DataBase(ConfigurationManager.ConnectionStrings["t26"].ConnectionString);

			// først chr_samlet
			string sql = "insert into chr_samlet(chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, geom) values ";
			string val = "";
			int nr = 0;
			try
			{
				ud.BeginTransaction();
				ud.SetSQLStatement("truncate chr_samlet");
				//skrivLog("t26", ud.GetSQLStatement());
				ud.Execute();
				db.SetSQLStatement("select chr_nr, bes_nr, bruger, dyreart, virk_art, virk_tekst, antal_dyr1, antal_dyr2, antal_dyr4, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, geom from chr.chr_samlet");
				db.Query();
				foreach (DataRow r in db.Result.Rows)
				{
					nr++;
					if (val != "") val += ",";
					val += fyld_kolonner(ref db, r);

					if (nr % 100 == 0 || nr == db.Result.Rows.Count )
					{
						ud.SetSQLStatement(sql + val);
						////skrivLog("t26", ud.GetSQLStatement());
						ud.Execute();
						val = "";
					}
				}
				// og så dyreart - t26-db er så gammel, at den ikke understøtter string_agg
				// så slet det hele og indsæt en komplet tabel
				//string dyr = ud.SingleSelect<string>("select string_agg(id::text, ',') as ids from dyreart");
				////skrivLog("t26", ud.GetSQLStatement());
				//if (dyr != null && dyr != "")
				//{
				//	db.SetSQLStatement("select * from chr.dyrearter where id not in(" + dyr + ")");
				//	skrivLog("t26", db.GetSQLStatement());
				//	db.Query();
				//	foreach (DataRow r in db.Result.Rows)
				//	{
				//		ud.SetSQLStatement("insert into dyrearter(dyreartkode, dyrearttekst) values(" + r["id"].ToString() + ",'" + r["navn"].ToString() + "')");
				//		//skrivLog("t26", ud.GetSQLStatement());
				//		ud.Execute();
				//	}
				//}
				ud.SetSQLStatement("truncate dyreart");
				//skrivLog("t26", ud.GetSQLStatement());
				ud.Execute();
				sql = "insert into dyreart(id, dyreartkode, dyrearttekst) values ";
				db.SetSQLStatement("select id, navn from chr.dyrearter");
				skrivLog("t26", db.GetSQLStatement());
				db.Query();
				foreach (DataRow r in db.Result.Rows)
				{
					val += ",(" + r["id"].ToString() + "," + r["id"].ToString() + ",'" + r["navn"].ToString() + "')";
				}
				ud.SetSQLStatement(sql + val.Substring(1));
				//skrivLog("t26", ud.GetSQLStatement());
				ud.Execute();


				// og endelig de ophoerte 
				// de kan ikke ændres, men måske forsvinde, og i hvert fald tilføjes
				// derfor er det hurtigste nok at slå id'er op og se om der skal fjernes nogen eller tilføjes nogen
				// slet forsvundne

				// ??? dette bør gøres hurtigere, "not in" er altid meget langsom
				string chr = db.SingleSelect<string>("select string_agg(chr_nr::text, ',') as ids from chr.chr_ophoert where geom is not null");
				skrivLog("t26", db.GetSQLStatement());
				if (chr != null && chr != "")
				{
					ud.SetSQLStatement("delete from chr_ophoert where chr_nr not in(" + chr + ")");
					//skrivLog("t26", ud.GetSQLStatement());
					ud.Execute();
				}


				// ??? dette bør gøres hurtigere, "not in" er altid meget langsom
				ud.SetSQLStatement("select chr_nr from chr_ophoert");
				//skrivLog("t26", ud.GetSQLStatement());
				ud.Query();
				foreach (DataRow r in ud.Result.Rows)
				{
					chr += "," + r["chr_nr"].ToString();
				}
				if (chr != "")
				{
					chr = " where chr_nr not in(" + chr.Substring(1) + ")";
				}
				db.SetSQLStatement("select chr_nr, bruger, dyreart, virk_art, virk_tekst, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, dato_ophoer, geom from chr.chr_ophoert " + chr);
				skrivLog("t26", db.GetSQLStatement());
				db.Query();
				nr = 0;
				val = "";
				sql = "insert into chr_ophoert(chr_nr, bruger, dyreart, virk_art, virk_tekst, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, dato_ophoer, geom) values "; 
				foreach (DataRow r in db.Result.Rows)
				{
					nr++;
					if (val != "") val += ",";
					val += fyld_kolonner(ref db, r);

					if (nr % 100 == 0 || nr == db.Result.Rows.Count)
					{
						ud.SetSQLStatement(sql + val);
						//skrivLog("t26", ud.GetSQLStatement());
						ud.Execute();
						val = "";
					}
				}
	
				ud.CommitTransaction();
			}
			catch (Exception e)
			{
				ud.RollbackTransaction();
				skrivLog("indlæsning fejlet: ", e.Message + "\r\nStacktrace: " + e.StackTrace);
				fejl = true;
			}
			ud.Close();
			db.Close();
		}
		static void kopier_test()
		{
			DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);
			DataBase ud = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);

			string val = "";
			int nr = 0;
			try
			{
				ud.BeginTransaction();
				// og endelig de ophoerte 
				// de kan ikke ændres, men måske forsvinde, og i hvert fald tilføjes
				// derfor er det hurtigste nok at slå id'er op og se om der skal fjernes nogen eller tilføjes nogen
				// slet forsvundne
				string chr = db.SingleSelect<string>("select string_agg(chr_nr::text, ',') as ids from chr.chr_ophoert");
				if (chr != "")
				{
					ud.SetSQLStatement("delete from chr_ophoert where chr_nr not in(" + chr + ")");
					ud.Execute();
				}
				// og så eventuelle nye
				chr = ud.SingleSelect<string>("select string_agg(chr_nr::text, ',') as ids from chr_ophoert");
				if (chr == null) 
					chr = "";
				if (chr != "")
				{
					chr = "where id not in(" + chr + ")";
				}
				db.SetSQLStatement("select chr_nr, bruger, dyreart, virk_art, virk_tekst, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, dato_ophoer, geom from chr.chr_ophoert " + chr);
				db.Query();
				nr = 0;
				val = "";
				string sql = "insert into chr_ophoert(chr_nr, bruger, dyreart, virk_art, virk_tekst, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor, x_koor, y_koor, dato_ophoer, geom) values ";
				foreach (DataRow r in db.Result.Rows)
				{
					nr++;
					if (val != "") val += ",";
					val += fyld_kolonner(ref db, r);

					//if (nr % 100 == 0 || nr == db.Result.Rows.Count)
					//{
						ud.SetSQLStatement(sql + val);
						ud.Execute();
						val = "";
					//}
				}


				ud.CommitTransaction();
			}
			catch
			{
				ud.RollbackTransaction();
			}
			ud.Close();
			db.Close();
		}

		static void opdater_tæthed()
		{
			skrivLog("opdater_tæthed");
			// dette giver kun mening atkøre hvis der er ændringer i chr_samlet - det er tjekket inden funktionen starter

			DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["fvst_chr"].ConnectionString);
			
			
			
			int rk = 0;
			try
			{
				db.BeginTransaction();
				// slet ophørte chr_nr fra chr_kom
				db.SetSQLStatement("delete from chr.chr_kom a where not exists(select chr_nr from chr.chr_samlet where chr_nr=a.chr_nr)");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				// ændrede geometrier i eksisterende chr-numre
				db.SetSQLStatement("select distinct s.chr_nr, s.geom from chr.chr_samlet s, chr.chr_kom k where s.chr_nr=k.chr_nr and s.geom is not null and s.geom::text <> k.geom::text");
				//skrivLog("chr", db.GetSQLStatement());
				db.Query();
				if (db.Result != null && db.Result.Rows.Count > 0)
				{
					skrivLog("ændret geometri", db.Result.Rows.Count.ToString());
					//skrivLog("ændret geometri " + db.Result.Rows.Count.ToString());
					DataTable tbl = db.Result;
					rk = 0;
					foreach (DataRow r in tbl.Rows)
					{
						rk++;
						if (rk % 10 == 0)
							skrivLog("ændret geom " + rk.ToString());
						// slå kommunen op i dagi.dagi_10_kommune
						db.SetSQLStatement("update chr.chr_kom set komnr = q.komnr, geom= '" + r["geom"] + "' from (select komnr from dagi.dagi_10_kommune where st_contains(geom, '" + r["geom"] + "')) q where chr_nr = " + r["chr_nr"].ToString());
						//skrivLog("chr", db.GetSQLStatement());
						db.Execute();
					}
				}

				skrivLog("finder nye");
				db.SetSQLStatement("select distinct a.chr_nr, a.geom from chr.chr_samlet a where a.geom is not null and not exists (select chr_nr from chr.chr_kom where chr_nr=a.chr_nr)");
				//skrivLog("chr", db.GetSQLStatement());
				db.Query();
				if (db.Result != null && db.Result.Rows.Count > 0)
				{
					skrivLog("opdaterer chr_kom", "nye " + db.Result.Rows.Count.ToString());
					skrivLog("nye " + db.Result.Rows.Count.ToString());
					DataTable tbl = db.Result;
					rk = 0;
					foreach (DataRow r in tbl.Rows)
					{
						rk++;
						//if (rk % 10 == 0)
						skrivLog("nye kom " + rk.ToString());
						// slå kommunen op i dagi.dagi_10_kommune
						db.SetSQLStatement("insert into chr.chr_kom (chr_nr, komnr, geom) values(" + r["chr_nr"] + ", (select komnr from dagi.dagi_10_kommune where st_contains(geom, '" + r["geom"] + "')), '" + r["geom"] + "') ");
						//skrivLog("chr", db.GetSQLStatement());
						db.Execute();
					}
				}
				// overfør komnr til dem, hvor komnr er null i chr_kom
				db.SetSQLStatement("update chr.chr_kom a set komnr = b.komnr from chr.chr_samlet b where a.chr_nr=b.chr_nr and a.komnr is null");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();



				// og så skal der beregnes antal og kommunetæthed
				// det bliver meget nemmere hvis der altid er 0 i stedet for null i antal-felterne
				db.SetSQLStatement("update chr.chr_samlet set antal_dyr1 =0 where antal_dyr1 is null");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				db.SetSQLStatement("update chr.chr_samlet set antal_dyr2 =0 where antal_dyr2 is null");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				db.SetSQLStatement("update chr.chr_samlet set antal_dyr3 =0 where antal_dyr3 is null");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				db.SetSQLStatement("update chr.chr_samlet set antal_dyr4 =0 where antal_dyr4 is null");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				// først antal - tøm tabellen, tilføj alle chr-numre og dyrearter fra chr_samlet og opdater med kommunenummer
				skrivLog("opdaterer chr_antal", "");
				db.SetSQLStatement("truncate chr.chr_antal");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();



				// totalantallet skal beregnes noget anderledes, der er forskellige regler for de forskellige dyrearter
				// 2, hvis udfyldt, ellers 1 - dyreart 10
				db.SetSQLStatement("insert into chr.chr_antal (chr_nr, dyreart, antal) select q.chr_nr, q.dyreart, sum(q.antal) from (select chr_nr, dyreart, case antal_dyr2 when 0 then antal_dyr1 else antal_dyr2 end as antal from chr.chr_samlet where geom is not null and dyreart =10) q group by 1,2");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				// 3, hvis udfyldt, ellers 1 + 2 + 4 dyreart 50
				db.SetSQLStatement("insert into chr.chr_antal (chr_nr, dyreart, antal) select q.chr_nr, q.dyreart, sum(q.antal) from (select chr_nr, dyreart, case antal_dyr3 when 0 then antal_dyr1 + antal_dyr2 + antal_dyr4 else antal_dyr3 end as antal from chr.chr_samlet where geom is not null and dyreart =50) q group by 1,2");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				// 2 - dyrearterne 12,13,14,21,23,24,25,28,30,31,32,33,34,35,40,41,42,43,44,45,46,47,48,49
				db.SetSQLStatement("insert into chr.chr_antal (chr_nr, dyreart, antal) select chr_nr, dyreart, sum(antal_dyr2) as antal from chr.chr_samlet where geom is not null and dyreart in (12,13,14,21,23,24,25,28,30,31,32,33,34,35,40,41,42,43,44,45,46,47,48,49) group by 1,2");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				// 1 + 2 + 4 - dyrearterne 15,51,52,53,54,57,58,59,60,61
				db.SetSQLStatement("insert into chr.chr_antal (chr_nr, dyreart, antal) select chr_nr, dyreart, sum(antal_dyr1 + antal_dyr2 + antal_dyr4) as antal from chr.chr_samlet where geom is not null and dyreart in (15,51,52,53,54,57,58,59,60,61) group by 1,2");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				// 3 - dyrearterne 55,56 - og efter opdeling af toskallede i muslinger og østers 62,63
				db.SetSQLStatement("insert into chr.chr_antal (chr_nr, dyreart, antal) select chr_nr, dyreart, sum(antal_dyr3)  as antal from chr.chr_samlet where geom is not null and dyreart in (55,56,62,63) group by 1,2");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();
				// skal de aggregerede antal =0 slettes igen? Nej, der er en signatur til Ingen dyr

				// tilføj komnr
				db.SetSQLStatement("update chr.chr_antal a set komnr = k.komnr from chr.chr_kom k where a.chr_nr=k.chr_nr and k.komnr is not null ");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				// og endelig chr_kom_taethed
				skrivLog("opdaterer chr_kom_taethed", "");
				// slet alle poster, hvor dyreart er > -1 (de faste kan lige så godt blive liggende

				db.SetSQLStatement("delete from chr.chr_kom_taethed where dyreart > -1");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				db.SetSQLStatement("insert into chr.chr_kom_taethed (komnr, dyreart, antal) select komnr, dyreart, sum(antal) from chr.chr_antal where komnr >0 and antal >0 group by komnr, dyreart;");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				db.SetSQLStatement("update chr.chr_kom_taethed a set taethed = a.antal / k.areal from dagi.dagi_10_kommune k where a.komnr=k.komnr;");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute();

				// commit
				db.CommitTransaction();

				// vacuum
				skrivLog("vacuum");
				skrivLog("vacuum", "chr_samlet");
				db.SetSQLStatement("VACUUM FULL chr.chr_samlet");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute(false);
				skrivLog("vacuum", "chr_ophoert");
				db.SetSQLStatement("VACUUM FULL chr.chr_ophoert");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute(false);
				skrivLog("vacuum", "chr_kom");
				db.SetSQLStatement("VACUUM FULL chr.chr_kom");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute(false);
				skrivLog("vacuum", "chr_antal");
				db.SetSQLStatement("VACUUM FULL chr.chr_antal");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute(false);
				skrivLog("vacuum", "chr_kom_taethed");
				db.SetSQLStatement("VACUUM FULL chr.chr_kom_taethed");
				//skrivLog("chr", db.GetSQLStatement());
				db.Execute(false);

				skrivLog("slut", "\r\n");
			}
			catch (Exception ex)
			{
				db.RollbackTransaction();
				skrivLog("Fejl: " + ex.Message, ex.StackTrace);
				skrivLog("SQL: " + db.GetSQLStatement());
				fejl = true;
			}
			finally
			{
				db.Close();
			}
		}

		//static void overfør_tabeller()
		//{
		//	// overfør de rettede tabeller til andre servere, hvis det er nødvendigt

		//	// chr benyttes aktuelt på den gamle server, som kun kan tilgås fra LE34s hoved-IP
		//	// den er der vel ingen grund til at opdatere her, når dette og fugleinfluenzaen er på plads, skal den gamle server tages ud af drift
		//	// indtil da kan den gamle server hente sine egne data via avi2006

		//	// og så er der serverne hos LE34
		//	// fvst-test kan ikke nødvendigvis se srv-postgis, og måske skal den flyttes til T26, så den bør have sine egne data
		//	// srv-postgis indeholder andre L&F tabeller, der spiller sammen med disse, så det er også nødvendigt at have data her

		//	// koden er skrevet til skema-strukturen på fvst-test

		//}
		
		private static void skrivLog(string tekst, string parameter ="")
		{
			// skriv først i konsol-vindue
			Console.WriteLine(tekst + "\t" + parameter);
			StreamWriter wr;

			// Open the file directly using StreamWriter. 
			try
			{
				wr = new StreamWriter(logfil, true);
			}
			catch //(IOException exc)
			{
				// throw new Exception("Der er sket en fejl i skrivLog:\r\n" + exc.Message);
				return;
			}
			if (tekst == "programstart")
				wr.WriteLine("");
			wr.WriteLine(DateTime.Now.ToString() + "\t" + tekst + "\t" + parameter);
			wr.Close();
		}
	}
}



/* Og hvordan skal vi så overføre temp-tabellerne til de rigtige
 * dyrearter og virksomhedsarter bruger vi egentlig ikke, gør ingenting
 
 * ophørte
 * her kan der vel ikke være nogen der fjernes, kun nye der skal tilføjes
 * 
 *  aktive
 *  tøm tabellen og læg alle de nye ind, det er det enkleste og sikreste
 *  
 * dyrearter  --- skal faktisk bruges hos l & f, som aktuelt ligger på srv-postgis, og som bruges sammen med andre tabeller dér, nemlig chr_radius som også findes her (værktøjet er også tilgængeligt for fvst, eller skal i hvert fald være det)

 
 */