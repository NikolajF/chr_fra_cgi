﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.0
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-07-11 23:14:41

SET statement_timeout = 0;
--SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = chr, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 621422)
-- Name: chr_antal; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_antal (
    id integer NOT NULL,
    chr_nr integer NOT NULL,
    komnr integer DEFAULT 0 NOT NULL,
    dyreart integer NOT NULL,
    antal integer
);


ALTER TABLE chr_antal OWNER TO postgres;

SET default_with_oids = true;

--
-- TOC entry 208 (class 1259 OID 621426)
-- Name: chr_samlet; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_samlet (
    id integer NOT NULL,
    chr_nr integer,
    bruger character varying(244),
    dyreart integer,
    virk_tekst character varying(80),
    antal_dyr1 integer DEFAULT 0,
    antal_dyr2 integer DEFAULT 0,
    antal_dyr4 integer DEFAULT 0,
    vej character varying(80),
    bynavn character varying(70),
    postnr integer,
    postby character varying(70),
    stald_x_koor double precision,
    stald_y_koor double precision,
    x_koor double precision,
    y_koor double precision,
    geom geometry,
    virk_art integer,
    bes_nr integer,
    antal_dyr3 integer DEFAULT 0,
    CONSTRAINT enforce_dims_geom CHECK ((st_ndims(geom) = 2)),
    CONSTRAINT enforce_geotype_geom CHECK (((geometrytype(geom) = 'POINT'::text) OR (geom IS NULL))),
    CONSTRAINT enforce_srid_geom CHECK ((st_srid(geom) = 25832))
);


ALTER TABLE chr_samlet OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 621442)
-- Name: chr_antal_id_seq; Type: SEQUENCE; Schema: chr; Owner: postgres
--

CREATE SEQUENCE chr_antal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chr_antal_id_seq OWNER TO postgres;

--
-- TOC entry 4325 (class 0 OID 0)
-- Dependencies: 210
-- Name: chr_antal_id_seq; Type: SEQUENCE OWNED BY; Schema: chr; Owner: postgres
--

ALTER SEQUENCE chr_antal_id_seq OWNED BY chr_antal.id;


SET default_with_oids = false;

--
-- TOC entry 216 (class 1259 OID 621480)
-- Name: chr_kom; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_kom (
    chr_nr integer NOT NULL,
    komnr integer,
    geom geometry,
    CONSTRAINT enforce_dims_geom CHECK ((st_ndims(geom) = 2)),
    CONSTRAINT enforce_geotype_geom CHECK (((geometrytype(geom) = 'POINT'::text) OR (geom IS NULL))),
    CONSTRAINT enforce_srid_geom CHECK ((st_srid(geom) = 25832))
);


ALTER TABLE chr_kom OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 621486)
-- Name: chr_kom_taethed; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_kom_taethed (
    id integer NOT NULL,
    komnr integer NOT NULL,
    dyreart integer NOT NULL,
    taethed double precision DEFAULT 0 NOT NULL,
    antal integer DEFAULT 0 NOT NULL
);


ALTER TABLE chr_kom_taethed OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 621491)
-- Name: chr_kom_taethed_id_seq; Type: SEQUENCE; Schema: chr; Owner: postgres
--

CREATE SEQUENCE chr_kom_taethed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chr_kom_taethed_id_seq OWNER TO postgres;

--
-- TOC entry 4326 (class 0 OID 0)
-- Dependencies: 218
-- Name: chr_kom_taethed_id_seq; Type: SEQUENCE OWNED BY; Schema: chr; Owner: postgres
--

ALTER SEQUENCE chr_kom_taethed_id_seq OWNED BY chr_kom_taethed.id;


--
-- TOC entry 219 (class 1259 OID 621493)
-- Name: chr_ophoert; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_ophoert (
    chr_nr integer,
    bruger character varying(244),
    dyreart integer,
    virk_tekst character varying(80),
    vej character varying(80),
    bynavn character varying(70),
    postnr integer,
    postby character varying(70),
    stald_x_koor double precision,
    stald_y_koor double precision,
    x_koor double precision,
    y_koor double precision,
    id integer NOT NULL,
    virk_art integer,
    dato_ophoer date,
    geom geometry,
    CONSTRAINT enforce_dims_geom CHECK ((st_ndims(geom) = 2)),
    CONSTRAINT enforce_geotype_geom CHECK (((geometrytype(geom) = 'POINT'::text) OR (geom IS NULL))),
    CONSTRAINT enforce_srid_geom CHECK ((st_srid(geom) = 25832))
)
WITH (autovacuum_enabled=true);


ALTER TABLE chr_ophoert OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 621499)
-- Name: chr_ophoert_id_seq; Type: SEQUENCE; Schema: chr; Owner: postgres
--

CREATE SEQUENCE chr_ophoert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chr_ophoert_id_seq OWNER TO postgres;

--
-- TOC entry 4327 (class 0 OID 0)
-- Dependencies: 220
-- Name: chr_ophoert_id_seq; Type: SEQUENCE OWNED BY; Schema: chr; Owner: postgres
--

ALTER SEQUENCE chr_ophoert_id_seq OWNED BY chr_ophoert.id;


--
-- TOC entry 221 (class 1259 OID 621501)
-- Name: chr_radius; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE chr_radius (
    id integer NOT NULL,
    navn character varying,
    chr character varying,
    dyrearter character varying,
    dato_rettet timestamp without time zone,
    radius integer,
    dato_oprettet timestamp without time zone,
    dyrearter_tekst character varying,
    customer_id integer
);


ALTER TABLE chr_radius OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 621507)
-- Name: chr_radius_id_seq; Type: SEQUENCE; Schema: chr; Owner: postgres
--

CREATE SEQUENCE chr_radius_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chr_radius_id_seq OWNER TO postgres;

--
-- TOC entry 4328 (class 0 OID 0)
-- Dependencies: 222
-- Name: chr_radius_id_seq; Type: SEQUENCE OWNED BY; Schema: chr; Owner: postgres
--

ALTER SEQUENCE chr_radius_id_seq OWNED BY chr_radius.id;


--
-- TOC entry 223 (class 1259 OID 621509)
-- Name: chr_samlet_id_seq1; Type: SEQUENCE; Schema: chr; Owner: postgres
--

CREATE SEQUENCE chr_samlet_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chr_samlet_id_seq1 OWNER TO postgres;

--
-- TOC entry 4329 (class 0 OID 0)
-- Dependencies: 223
-- Name: chr_samlet_id_seq1; Type: SEQUENCE OWNED BY; Schema: chr; Owner: postgres
--

ALTER SEQUENCE chr_samlet_id_seq1 OWNED BY chr_samlet.id;


--
-- TOC entry 430 (class 1259 OID 1145679)
-- Name: dyrearter; Type: TABLE; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE TABLE dyrearter (
    id integer NOT NULL,
    navn character varying,
    aktiv boolean DEFAULT true,
    chr_aktuel boolean
);


ALTER TABLE dyrearter OWNER TO postgres;


SET search_path = chr, pg_catalog;

--
-- TOC entry 4125 (class 2604 OID 622450)
-- Name: id; Type: DEFAULT; Schema: chr; Owner: postgres
--

ALTER TABLE ONLY chr_antal ALTER COLUMN id SET DEFAULT nextval('chr_antal_id_seq'::regclass);


--
-- TOC entry 4137 (class 2604 OID 622451)
-- Name: id; Type: DEFAULT; Schema: chr; Owner: postgres
--

ALTER TABLE ONLY chr_kom_taethed ALTER COLUMN id SET DEFAULT nextval('chr_kom_taethed_id_seq'::regclass);


--
-- TOC entry 4138 (class 2604 OID 622452)
-- Name: id; Type: DEFAULT; Schema: chr; Owner: postgres
--

ALTER TABLE ONLY chr_ophoert ALTER COLUMN id SET DEFAULT nextval('chr_ophoert_id_seq'::regclass);


--
-- TOC entry 4139 (class 2604 OID 622453)
-- Name: id; Type: DEFAULT; Schema: chr; Owner: postgres
--

ALTER TABLE ONLY chr_radius ALTER COLUMN id SET DEFAULT nextval('chr_radius_id_seq'::regclass);


--
-- TOC entry 4130 (class 2604 OID 622454)
-- Name: id; Type: DEFAULT; Schema: chr; Owner: postgres
--

ALTER TABLE ONLY chr_samlet ALTER COLUMN id SET DEFAULT nextval('chr_samlet_id_seq1'::regclass);


SET search_path = dagi, pg_catalog;

--
-- TOC entry 4140 (class 2604 OID 622455)
-- Name: ogc_fid; Type: DEFAULT; Schema: dagi; Owner: postgres
--

ALTER TABLE ONLY dagi_10_kommune ALTER COLUMN ogc_fid SET DEFAULT nextval('dagi_10_kommune_ogc_fid_seq'::regclass);


SET search_path = chr, pg_catalog;

--
-- TOC entry 4167 (class 2606 OID 1145687)
-- Name: dyrearter_pkey; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dyrearter
    ADD CONSTRAINT dyrearter_pkey PRIMARY KEY (id);


--
-- TOC entry 4146 (class 2606 OID 1145214)
-- Name: pk_chr_antal; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chr_antal
    ADD CONSTRAINT pk_chr_antal PRIMARY KEY (id);


--
-- TOC entry 4155 (class 2606 OID 1145216)
-- Name: pk_chr_kom; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chr_kom
    ADD CONSTRAINT pk_chr_kom PRIMARY KEY (chr_nr);


--
-- TOC entry 4159 (class 2606 OID 1145218)
-- Name: pk_chr_kom_taethed; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chr_kom_taethed
    ADD CONSTRAINT pk_chr_kom_taethed PRIMARY KEY (id);


--
-- TOC entry 4162 (class 2606 OID 1145220)
-- Name: pk_chr_ophoert; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chr_ophoert
    ADD CONSTRAINT pk_chr_ophoert PRIMARY KEY (id);


--
-- TOC entry 4152 (class 2606 OID 1145222)
-- Name: pk_chr_samlet; Type: CONSTRAINT; Schema: chr; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chr_samlet
    ADD CONSTRAINT pk_chr_samlet PRIMARY KEY (id);


SET search_path = dagi, pg_catalog;

--
-- TOC entry 4165 (class 2606 OID 1145224)
-- Name: dagi_10_kommune_pk; Type: CONSTRAINT; Schema: dagi; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dagi_10_kommune
    ADD CONSTRAINT dagi_10_kommune_pk PRIMARY KEY (ogc_fid);


SET search_path = chr, pg_catalog;

--
-- TOC entry 4142 (class 1259 OID 1145420)
-- Name: chr_antal_chr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_antal_chr ON chr_antal USING btree (chr_nr);


--
-- TOC entry 4143 (class 1259 OID 1145421)
-- Name: chr_antal_dyreart; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_antal_dyreart ON chr_antal USING btree (dyreart);


--
-- TOC entry 4144 (class 1259 OID 1145422)
-- Name: chr_antal_komnr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_antal_komnr ON chr_antal USING btree (komnr);


--
-- TOC entry 4153 (class 1259 OID 1145427)
-- Name: chr_kom_komnr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_kom_komnr ON chr_kom USING btree (komnr);


--
-- TOC entry 4156 (class 1259 OID 1145428)
-- Name: chr_kom_taethed_dyreart; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_kom_taethed_dyreart ON chr_kom_taethed USING btree (dyreart);


--
-- TOC entry 4157 (class 1259 OID 1145429)
-- Name: chr_kom_taethed_komnr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX chr_kom_taethed_komnr ON chr_kom_taethed USING btree (komnr);


--
-- TOC entry 4147 (class 1259 OID 1145431)
-- Name: i_bes_nr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX i_bes_nr ON chr_samlet USING btree (bes_nr);


--
-- TOC entry 4148 (class 1259 OID 1145432)
-- Name: i_chr_nr; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX i_chr_nr ON chr_samlet USING btree (chr_nr);


--
-- TOC entry 4149 (class 1259 OID 1145433)
-- Name: i_dyreart; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX i_dyreart ON chr_samlet USING btree (dyreart);


--
-- TOC entry 4150 (class 1259 OID 1145664)
-- Name: i_staldkoord; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX i_staldkoord ON chr_samlet USING btree (stald_x_koor, stald_y_koor);


--
-- TOC entry 4160 (class 1259 OID 1145434)
-- Name: ix_chr_ophoert_geom; Type: INDEX; Schema: chr; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_chr_ophoert_geom ON chr_ophoert USING gist (geom);


SET search_path = dagi, pg_catalog;

--
-- TOC entry 4163 (class 1259 OID 1145435)
-- Name: dagi_10_kommune_geom_idx; Type: INDEX; Schema: dagi; Owner: postgres; Tablespace: 
--

CREATE INDEX dagi_10_kommune_geom_idx ON dagi_10_kommune USING gist (geom_3d);


-- Completed on 2015-07-11 23:14:41

--
-- PostgreSQL database dump complete
--

